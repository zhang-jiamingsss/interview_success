import { auth, http, logger, UserStoreKey } from '../commons/utils'
import { User } from '../models'
import { HcLoadingDialog, HcNavBar } from '../commons/components'
import { promptAction } from '@kit.ArkUI'
import { photoAccessHelper } from '@kit.MediaLibraryKit'
import { fileIo } from '@kit.CoreFileKit'
import { FormData } from '@ohos/axios'
import { image } from '@kit.ImageKit'

@Entry
@Component
struct ProfilePage {
  @StorageProp(UserStoreKey) user: User = {} as User
  dialog: CustomDialogController = new CustomDialogController({
    builder: HcLoadingDialog({ message: '更新中...' }),
    customStyle: true,
    alignment: DialogAlignment.Center
  })

  async updateNickName() {
    await http.request<null, User>({
      url: 'userInfo/profile',
      method: 'post',
      data: { nickName: this.user.nickName } as User
    })
    promptAction.showToast({ message: '修改昵称成功' })
    auth.setUser(this.user)
  }

  async updateAvatar() {
    // 1. 打开相册选一张图
    // 选择图片的参数
    const options = new photoAccessHelper.PhotoSelectOptions()
    // 指定文件类型是图片
    options.MIMEType = photoAccessHelper.PhotoViewMIMETypes.IMAGE_TYPE
    // 选择一张图
    options.maxSelectNumber = 1
    // 创建相册选择器
    const picker = new photoAccessHelper.PhotoViewPicker()
    // 去选图片
    const selectResult = await picker.select(options)
    // 得到选择图片的 uri 完整路径
    const uri = selectResult.photoUris[0]

    // 2. 复制这张图片到沙箱 cache 目录
    // // 图片存放路径
    // const file = fileIo.openSync(uri, fileIo.OpenMode.READ_ONLY)
    // const filePath = getContext(this)
    //   .cacheDir + '/' + file.name
    // // 原文件资源的路径|FD   生成图片的路径
    // fileIo.copyFileSync(file.fd, filePath)
    // // 3. 上传图片，上传文件
    // // 这是axios上传图片需要的对象类型 FormData
    // const formData = new FormData()
    // // 添加一个名字叫 file 的数据，数据需要 `internal://cache/在沙箱目录的文件地址`
    // formData.append('file', `internal://cache/${file.name}`)
    // // 关闭文件对象，需要再使用完毕后去操作
    // fileIo.closeSync(file)

    // 2. 相册中的图片需要压缩后上传
    // 2.1 根据相册图片生成 ImageSource 对象
    const originFile = fileIo.openSync(uri, fileIo.OpenMode.READ_ONLY)
    const imageSource = image.createImageSource(originFile.fd)
    // 2.2 使用 packing 压缩图片，二进制图片数据
    const imagePacker = image.createImagePacker()
    const arrayBuffer = await imagePacker.packing(imageSource, { format: 'image/jpeg', quality: 70 })
    // 2.3 存储到沙箱目录
    const newFilePath = getContext(this)
      .cacheDir + '/' + originFile.name
    const newFile = fileIo.openSync(newFilePath, fileIo.OpenMode.CREATE | fileIo.OpenMode.READ_WRITE)
    fileIo.writeSync(newFile.fd, arrayBuffer)
    // 2.4 创建一个 formData
    const formData = new FormData()
    formData.append('file', `internal://cache/${newFile.name}`)
    logger.debug('newFile', fileIo.statSync(newFile.fd)
      .size
      .toString())
    fileIo.closeSync(newFile.fd)

    this.dialog.open()
    await http.request<null>({
      url: 'userInfo/avatar',
      method: 'post',
      headers: {
        "Content-Type": 'multipart/form-data'
      },
      data: formData,
      // 需要 context
      context: getContext(this)
    })
    // 上传成功 + 更新头像
    // 1. 请求后端最新的用户信息，包含头像
    // 2. 更新本地的头像
    const user = await http.request<User>({ url: 'userInfo' })
    this.user.avatar = user.avatar
    auth.setUser(this.user)
    this.dialog.close()
    promptAction.showToast({ message: '更新头像成功' })
  }

  build() {
    Column() {
      HcNavBar({ title: '编辑资料', showRightIcon: false })
      List() {
        ListItem() {
          Row() {
            Text('头像')
            Image(this.user.avatar || $r('app.media.ic_mine_avatar'))
              .alt($r('app.media.ic_mine_avatar'))
              .width(40)
              .aspectRatio(1)
              .borderRadius(20)
              .border({ width: 0.5, color: $r('app.color.common_gray_border') })
              .onClick(() => {
                this.updateAvatar()
              })
          }
          .width('100%')
          .height(60)
          .justifyContent(FlexAlign.SpaceBetween)
        }

        ListItem() {
          Row() {
            Text('昵称')
            TextInput({ text: this.user.nickName })
              .fontColor($r('app.color.common_gray_03'))
              .textAlign(TextAlign.End)
              .layoutWeight(1)
              .padding(0)
              .height(60)
              .backgroundColor($r('app.color.white'))
              .borderRadius(0)
              .onChange((value) => this.user.nickName = value)
              .onSubmit(() => {
                this.updateNickName()
              })
          }
          .width('100%')
          .justifyContent(FlexAlign.SpaceBetween)
        }
      }
      .width('100%')
      .height('100%')
      .padding({
        left: 35,
        right: 35,
        top: 15,
        bottom: 15
      })
      .divider({ strokeWidth: 0.5, color: $r('app.color.common_gray_bg') })
    }
  }
}