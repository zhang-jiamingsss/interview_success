//数据模型
export interface QuestionType {
  id: number
  name: string
  displayNewestFlag: 0 | 1
}

export interface QuestionItem {
  id: string;

  /* 题干 */
  stem: string;

  /* 难度 */
  difficulty: number;

  /* 点赞数 */
  likeCount: number;

  /* 浏览数 */
  views: number;

  /* 是否已看 */
  readFlag: 0 | 1;
}

// 排序类型
export enum SortType {
  Default = 0,
  DifficultyLow = 10,
  DifficultyHigh = 11,
  ViewLow = 20,
  ViewHigh = 21,
  Commend = 30
}

// 试题列表的请求参数类型
export interface QuestionListParams {
  type: number
  questionBankType: 9 | 10
  sort?: SortType
  page?: number
  pageSize?: number
  keyword?: string
}

// 试题分页响应数据的类型
export interface PageData<T> {
  total: number
  pageTotal: number
  rows: T[]
}

// 用户信息
export interface User {
  id: string
  username: string
  avatar: string
  token: string
  nickName?: string
  // 学习时长
  totalTime?: number
  // 打卡次数
  clockinNumbers?: number
}

export interface QuestionDetail extends QuestionItem {
  /* 答案 */
  answer: string
  /* 是否收藏 */
  collectFlag: 0 | 1
  /* 是否点赞 */
  likeFlag: 0 | 1
  /* 所属模块 */
  stage: string[]
}

export interface QuestionPageParams {
  item: QuestionItem
  list: QuestionItem[]
}

export interface WebPageParams {
  title: string
  src: string
}

//项目列表类型
export interface ProjectCompDatum {
  /**
   * 项目分类类别描述
   */
  describeInfo?: string;

  /**
   * 是否展示最新图标标识0不展示1展示
   */
  displayNewestFlag: number;

  /**
   * 项目类的分类图标（知识点的不返回）
   */
  icon?: string;

  /**
   * 主键id
   */
  id: number;

  /**
   * 分类名称
   */
  name: string;

  /**
   * 项目类返回标签信息
   */
  tags?: Tag[];

}

export interface Tag {
  /**
   * 边框颜色
   */
  borderColor: string;

  /**
   * 标签颜色
   */
  nameColor: string;

  /**
   * 标签名称
   */
  tagName: string;
}

//项目列表子页面类型
export interface IntData {
  /**
   * 总页数
   */
  pageTotal?: number;

  /**
   * 数据集合
   */
  rows?: interviewItem[];

  /**
   * 总数
   */
  total?: number;
}

export interface interviewItem {
  /**
   * 内容描述-面经有,试题列表没有
   */
  content: string;

  /**
   * 新增时间
   */
  createdAt?: string;

  /**
   * 面经创建者头像
   */
  creatorAvatar: string;

  /**
   * 创建者名称
   */
  creatorName: string;

  /**
   * 难度，难度-面试题需要显示此字段，注：1-2星是简单，3-4星是一般，5星是困难
   */
  difficulty?: number;

  /**
   * 题目id
   */
  id: string;

  /**
   * 点赞数量
   */
  likeCount: number;

  /**
   * 已点赞1 未点赞0
   */
  likeFlag: number;

  /**
   * 场景教案场景，双元教案1、核心提炼2、随堂测试3、随堂练习4、每日作业
   */
  planSceneName?: PlanSceneName;

  /**
   * 题号/面经的标题
   */
  questionNo?: string;

  /**
   * 题型：1单选，2多选，3判断，5简答，6代码，7实操
   */
  questionType?: number;

  /**
   * 是否已读标志，0未读1已读
   */
  readFlag: number;

  /**
   * 题干
   */
  stem: string;

  /**
   * 题干附件-面经列表的图片
   */
  stemAttachmentId?: string;

  /**
   * 学科
   */
  subjectName?: string;

  /**
   * 浏览量
   */
  views: number;
}

/**
 * 场景教案场景，双元教案1、核心提炼2、随堂测试3、随堂练习4、每日作业
 */
export enum PlanSceneName {
  Empty = "",
  其他 = "其他",
  核心提炼 = "核心提炼",
  每日作业 = "每日作业",
  随堂测试 = "随堂测试",
  随堂练习 = "随堂练习",
}

export interface QuestionOptParams {
  id: string
  /* 0 试题  2 面经 */
  type: 0 | 1
  /* 1 点赞  2 收藏 */
  optType: 1 | 2
}

export interface TimeItem {
  questionId: string
  startTime: number
  endTime: number
}

// 打卡，切换月份
export interface ClockInItem {
  id: string,
  createdAt: string
}

export interface ClockInfo {
  flag: boolean
  clockinNumbers: number
  totalClockinNumber: number
  clockins: ClockInItem[]
}

export interface ClockInfoParams {
  year: string,
  month: string
}

// 学习时间

export interface StudyTimeItem {
  id: string
  name: string
  total: number
  done: number
  undone: number
}

export interface StudyTimeCate {
  type: string,
  list: StudyTimeItem[]
}

export interface StudyTimeData {
  totalTime: number,
  studyData: StudyTimeCate[]
}